FROM ubuntu:18.04
LABEL maintainer=dataportal@edirex.ics.muni.cz

ARG geckodriver_tar_file=geckodriver-v0.26.0-linux64.tar.gz
RUN apt update && apt install -y python3-pip xvfb firefox x11-utils

ADD https://github.com/mozilla/geckodriver/releases/download/v0.26.0/${geckodriver_tar_file} /tmp
RUN tar -zxvf /tmp/${geckodriver_tar_file} --directory /usr/bin/ && \
    rm /tmp/${geckodriver_tar_file}

RUN mkdir /test-dataportal
COPY . /test-dataportal

WORKDIR /test-dataportal
RUN chmod +x test.py
RUN pip3 install -r /test-dataportal/requirements.txt

CMD ["./test.py"]
