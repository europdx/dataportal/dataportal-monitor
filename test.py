#!/usr/bin/env python3

import os
import time
import sys
import logging
from selenium import webdriver
from pyvirtualdisplay import Display


def main():

    if 'DEBUG_LEVEL' in os.environ and os.environ['DEBUG_LEVEL'] == 'debug':
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.ERROR)

    if 'LINK_PASSWD' in os.environ:
        lpasswd = os.environ['LINK_PASSWD']
    else:
#        logging.error("No password available in LINK_PASSWD environment variable.")
        print("UNKNOWN - No Linkedin password provided.")
        sys.exit(3)
     
    if 'LINK_USER' in os.environ:
        luser = os.environ['LINK_USER']
    else:
        luser = 'ics.ver@seznam.cz'

    try:

        display = Display(visible=0, size=(1200, 900))
        display.start()
        browser = webdriver.Firefox()
        browser.set_window_size(1200,900)
    except Exception as e:
        print("UNKNOWN - DataPortal test failed during test init with " + str(e))
        sys.exit(3)

    try:
        browser.get('https://dataportal.europdx.eu/search')
        logging.debug("-")

        link = browser.find_element_by_link_text('here')
        link.click()
        logging.debug("-")

        link = browser.find_element_by_link_text('Sign in with LinkedIn')
        link.click()
        logging.debug("-")

        inputElement = browser.find_element_by_id("username")
        inputElement.send_keys(luser)
        logging.debug("-")

        inputElement = browser.find_element_by_id("password")
        inputElement.send_keys(lpasswd)
        logging.debug("-")

        sub_but = browser.find_element_by_class_name("login__form")
        sub_but.submit()
        logging.debug("-")

        time.sleep(10)

        if browser.current_url != 'https://dataportal.europdx.eu/search':
            print("CRITICAL DataPortal test failed")
            sys.exit(2)

    except Exception as e:
        print("CRITICAL DataPortal test failed with " + str(e))
        sys.exit(2)

    print("OK - DataPortal test passed.")
    sys.exit(0)

if __name__ == '__main__':
    main()
