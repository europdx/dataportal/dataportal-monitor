# DataPortal-Monitor

Automatic testing of DataPortal's availability / functionality

Start with

```docker run -e LINK_PASSWD=xxx registry.gitlab.ics.muni.cz:443/europdx/dataportal/dataportal-monitor```

and customize with following environment variables:

*  **LINK_USER** - set user (optional)
*  **LINK_PASSWD** - set password
*  **DEBUG_LEVEL** - set to "debug" to get more debug information

In nrpe.cfg following can be used

``command[check_dataportal]=docker run  -e LINK_PASSWD=xxx --rm registry.gitlab.ics.muni.cz:443/europdx/dataportal/dataportal-monitor``
